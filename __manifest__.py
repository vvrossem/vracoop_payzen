{
    "name": "VRACOOP - Payzen configuration",
    "summary": "VRACOOP - Payzen configuration",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "website", "payment_payzen"
    ],
    "data": [
        # 'security/ir.model.access.csv',
        "views/payzen_views.xml",
    ]
}
