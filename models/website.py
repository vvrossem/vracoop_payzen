# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models, _
from ..helpers import constants


class VracWebsite(models.Model):
    _inherit = "website"

    payzen_site_id = fields.Char(
        string=_('Identifiant boutique'), 
        help=_('The identifier provided by PayZen.'), 
        default=constants.PAYZEN_PARAMS.get('SITE_ID'))
    payzen_key_test = fields.Char(
        string=_('Clé en mode test'), 
        help=_('Clé fournie par PayZen pour le mode test (disponible dans le Back Office PayZen).'), 
        default=constants.PAYZEN_PARAMS.get('KEY_TEST'), 
        readonly=constants.PAYZEN_PLUGIN_FEATURES.get('qualif'))
    payzen_key_prod = fields.Char(
        string=_('Clé en mode production'), 
        help=_('Clé fournie par PayZen (disponible dans le Back Office PayZen après activation du mode de production).'), 
        default=constants.PAYZEN_PARAMS.get('KEY_PROD'))
